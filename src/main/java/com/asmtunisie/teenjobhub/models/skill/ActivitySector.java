package com.asmtunisie.teenjobhub.models.skill;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "activitysector")
public class ActivitySector {

    @Id
    private String id;

    private String name;

    private int index;

    public ActivitySector() {
    }

    public ActivitySector(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
