package com.asmtunisie.teenjobhub.models.skill;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotBlank;
import java.util.ArrayList;
import java.util.List;

@Document(collection = "skills")
public class Skill {
    @Id
    private String id;

    @NotBlank
    private String name;

    @DBRef
    private List<ActivitySector> activitySector = new ArrayList<>();

    private double index;


    public Skill() {
    }

    public Skill(String name) {
        this.name = name;
    }

    public Skill(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ActivitySector> getActivitySector() {
        return activitySector;
    }

    public void setActivitySector(List<ActivitySector> activitySector) {
        this.activitySector = activitySector;
    }

    public double getIndex() {
        return index;
    }

    public void setIndex(double index) {
        this.index = index;
    }
}
