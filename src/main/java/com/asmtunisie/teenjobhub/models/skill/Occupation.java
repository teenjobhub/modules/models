package com.asmtunisie.teenjobhub.models.skill;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotBlank;

@Document(collection = "occupations")
public class Occupation {
    @Id
    private String id;

    @NotBlank
    private String name;


    @DBRef
    private ActivitySector activitySector;

    private double index;

    public Occupation() {

    }

    public Occupation(String name) {
        this.name= name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ActivitySector getActivitySector() {
        return activitySector;
    }

    public void setActivitySector(ActivitySector activitySector) {
        this.activitySector = activitySector;
    }

    public double getIndex() {
        return index;
    }

    public void setIndex(double index) {
        this.index = index;
    }
}
