package com.asmtunisie.teenjobhub.models.jobpost;

import com.asmtunisie.teenjobhub.models.usermodels.User;


public class Comment {
    private User user;
    private String name;

    public Comment(User user, String name) {
        this.user = user;
        this.name = name;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
