package com.asmtunisie.teenjobhub.models.jobpost;


import com.asmtunisie.teenjobhub.models.jobspace.JobSpace;
import com.asmtunisie.teenjobhub.models.usermodels.UserProfile;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotBlank;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Document(collection = "jobPosts")
public class JobPost {
    @Id
    private String id;
    private String jobPostTitle;
    @NotBlank
    private String jobPostDescription;
    @NotBlank
    private Date postDate;
    private String postImageUrl;
    @DBRef
    private UserProfile user;
    private List<Comment> comments = new ArrayList<>();
    @DBRef
    private JobSpace jobSpace;

    @DBRef
    private List<UserProfile> applicants = new ArrayList<>();

    private boolean open = true;

    @DBRef
    private UserProfile hiredUser;

    private String jobContract;
    private int jobRating;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getJobPostDescription() {
        return jobPostDescription;
    }

    public void setJobPostDescription(String jobPostDescription) {
        this.jobPostDescription = jobPostDescription;
    }

    public Date getPostDate() {
        return postDate;
    }

    public void setPostDate(Date postDate) {
        this.postDate = postDate;
    }

    public String getPostImageUrl() {
        return postImageUrl;
    }

    public void setPostImageUrl(String postImageUrl) {
        this.postImageUrl = postImageUrl;
    }

    public UserProfile getUser() {
        return user;
    }

    public void setUser(UserProfile user) {
        this.user = user;
    }



    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public JobSpace getJobSpace() {
        return jobSpace;
    }

    public void setJobSpace(JobSpace jobSpace) {
        this.jobSpace = jobSpace;
    }

    public List<UserProfile> getApplicants() {
        return applicants;
    }

    public void setApplicants(List<UserProfile> applicants) {
        this.applicants = applicants;
    }

    public boolean isOpen() {
        return open;
    }

    public void setOpen(boolean open) {
        this.open = open;
    }

    public UserProfile getHiredUser() {
        return hiredUser;
    }

    public void setHiredUser(UserProfile hiredUser) {
        this.hiredUser = hiredUser;
    }

    public String getJobPostTitle() {
        return jobPostTitle;
    }

    public void setJobPostTitle(String jobPostTitle) {
        this.jobPostTitle = jobPostTitle;
    }

    public String getJobContract() {
        return jobContract;
    }

    public void setJobContract(String jobContract) {
        this.jobContract = jobContract;
    }

    public int getJobRating() {
        return jobRating;
    }

    public void setJobRating(int jobRating) {
        this.jobRating = jobRating;
    }
}
