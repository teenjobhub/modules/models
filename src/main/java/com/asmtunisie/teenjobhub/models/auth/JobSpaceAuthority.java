package com.asmtunisie.teenjobhub.models.auth;

import com.asmtunisie.teenjobhub.models.jobspace.JobSpace;
import com.asmtunisie.teenjobhub.models.usermodels.Role;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.security.core.GrantedAuthority;

public class JobSpaceAuthority implements GrantedAuthority {

    @DBRef
    private JobSpace jobSpace;
    private Role role;

    public JobSpaceAuthority(JobSpace jobSpace, Role role) {
        this.jobSpace = jobSpace;
        this.role = role;
    }

    @Override
    public String getAuthority() {
        return role.name();
    }

    public JobSpace getJobSpace() {
        return jobSpace;
    }

    public void setJobSpace(JobSpace jobSpace) {
        this.jobSpace = jobSpace;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }


}
