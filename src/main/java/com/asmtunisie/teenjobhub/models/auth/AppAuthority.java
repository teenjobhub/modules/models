package com.asmtunisie.teenjobhub.models.auth;

public enum AppAuthority {
    ADD_USER,
    DELETE_USER,
    EDIT_USER,
    ADD_JOBS,
    EDIT_JOBS
}
