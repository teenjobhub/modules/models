package com.asmtunisie.teenjobhub.models.payload.request;

import com.asmtunisie.teenjobhub.models.auth.JobSpaceAuthority;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

public record SignupRequest(@NotBlank
                            @Size(min = 3, max = 20)
                            String username,
                            @NotBlank
                            @Size(max = 50)
                            @Email String email,
                            List<JobSpaceAuthority> roles,
                            @NotBlank
                            @Size(min = 6, max = 40)
                            String password) {
    public SignupRequest {
        roles = new ArrayList<>();
    }
}