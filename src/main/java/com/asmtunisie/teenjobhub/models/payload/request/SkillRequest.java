package com.asmtunisie.teenjobhub.models.payload.request;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class SkillRequest {
    @NotBlank
    @Size(min = 3, max = 20)
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
