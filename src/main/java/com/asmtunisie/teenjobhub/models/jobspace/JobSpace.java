package com.asmtunisie.teenjobhub.models.jobspace;

import com.asmtunisie.teenjobhub.models.usermodels.JobSpaceStaff;
import com.asmtunisie.teenjobhub.models.usermodels.User;
import com.asmtunisie.teenjobhub.models.usermodels.UserProfile;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.List;

@Document
public class JobSpace {
    @Id
    private String id;

    @NotBlank
    @Size(max = 20)
    private String name;


    @DBRef
    private UserProfile admin;


    private List<JobSpaceStaff> staff;

    public JobSpace() {
    }


    public JobSpace(String id, String name, UserProfile admin, List<JobSpaceStaff> staff) {
        this.id = id;
        this.name = name;
        this.admin = admin;
        this.staff = staff;
    }

    public JobSpace(String jobSpace) {
        this.name = jobSpace;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UserProfile getAdmin() {
        return admin;
    }

    public void setAdmin(UserProfile admin) {
        this.admin = admin;
    }

    public List<JobSpaceStaff> getStaff() {
        return staff;
    }

    public void setStaff(List<JobSpaceStaff> staff) {
        this.staff = staff;
    }
}
