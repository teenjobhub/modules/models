package com.asmtunisie.teenjobhub.models.usermodels;

import com.asmtunisie.teenjobhub.models.jobspace.JobSpace;
import com.asmtunisie.teenjobhub.models.skill.Occupation;
import com.asmtunisie.teenjobhub.models.skill.Skill;
import com.fasterxml.jackson.annotation.JsonBackReference;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;


@Document(collection = "profiles")
public class UserProfile {

    @Id
    private String id;
    @NotBlank
    @Size(max = 20)
    private String firstName;

    @NotBlank
    @Size(max = 50)
    @Email
    private String lastName;


    private String phoneNumber;

    @NotBlank
    @Size(max = 50)
    @Email
    private String country;

    @NotBlank
    @Size(max = 50)
    @Email
    private String governance;

    @DBRef(lazy = true)
    private User user ;

    @DBRef(lazy = true)
    private Occupation occupation ;

    @DBRef(lazy = true)
    private Set<Skill> skills = new HashSet<>();

    private String userImage;
    private String userSignature;

    private String userSignature;

    private double x;
    private double y;

    public void calculate() {
        this.x = occupation.getIndex();
        this.y = skills.stream().mapToDouble(Skill::getIndex).average().orElse(0);
    }
    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public UserProfile(String id) {
        this.id = id;
    }

    public UserProfile() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getGovernance() {
        return governance;
    }

    public void setGovernance(String governance) {
        this.governance = governance;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Occupation getOccupation() {
        return occupation;
    }

    public void setOccupation(Occupation occupation) {
        this.occupation = occupation;
    }

    public Set<Skill> getSkills() {
        return skills;
    }

    public void setSkills(Set<Skill> skills) {
        this.skills = skills;
    }
    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }


    public String getUserSignature() {
        return userSignature;
    }

    public void setUserSignature(String userSignature) {
        this.userSignature = userSignature;
    }
}
