package com.asmtunisie.teenjobhub.models.usermodels;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;


import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;


@Document(indexName = "profiles")
public class ElasticUserProfile {

    @Id
    private String id;
    @NotBlank
    @Size(max = 20)
    private String firstName;

    @NotBlank
    @Size(max = 50)
    @Email
    private String lastName;


    private String phoneNumber;

    @NotBlank
    @Size(max = 50)
    @Email
    private String country;

    @NotBlank
    @Size(max = 50)
    @Email
    private String governance;




    private String userImage;

    private double x;
    private double y;


    public ElasticUserProfile(String id) {
        this.id = id;
    }

    public ElasticUserProfile() {
    }

    public ElasticUserProfile(UserProfile userProfile) {
        this.id = userProfile.getId();
        this.firstName = userProfile.getFirstName();
        this.lastName = userProfile.getLastName();
        this.phoneNumber = userProfile.getPhoneNumber();
        this.country = userProfile.getCountry();
        this.governance = userProfile.getGovernance();
                this.userImage = userProfile.getUserImage();
        this.x = userProfile.getX();
        this.y = userProfile.getY();
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getGovernance() {
        return governance;
    }

    public void setGovernance(String governance) {
        this.governance = governance;
    }




    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }



}
