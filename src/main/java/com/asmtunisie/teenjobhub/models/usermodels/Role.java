package com.asmtunisie.teenjobhub.models.usermodels;

import com.asmtunisie.teenjobhub.models.auth.AppAuthority;

import java.util.Set;

public enum Role {
    ROLE_MODERATOR(Set.of(AppAuthority.EDIT_JOBS, AppAuthority.ADD_JOBS)),
    ROLE_ADMIN(Set.of(), true);

    private final Set<AppAuthority> authorities;
    private final boolean exclude;

    Role(Set<AppAuthority> authorities) {
        this.authorities = authorities;
        this.exclude = false;
    }

    Role(Set<AppAuthority> authorities, boolean exclude) {
        this.authorities = authorities;
        this.exclude = exclude;
    }

    public boolean hasAuthority(AppAuthority authority) {
        return exclude != authorities.contains(authority);
    }
}

