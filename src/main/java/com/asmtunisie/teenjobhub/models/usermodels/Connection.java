package com.asmtunisie.teenjobhub.models.usermodels;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;


@Document(collection = "network")
public class Connection {

    @Id
    private String id;

    private boolean pending = true;


    @DBRef
    private UserProfile originUser;

    @DBRef
    private UserProfile destinationUser;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isPending() {
        return pending;
    }

    public void setPending(boolean pending) {
        this.pending = pending;
    }

    public UserProfile getOriginUser() {
        return originUser;
    }

    public void setOriginUser(UserProfile originUser) {
        this.originUser = originUser;
    }

    public UserProfile getDestinationUser() {
        return destinationUser;
    }

    public void setDestinationUser(UserProfile destinationUser) {
        this.destinationUser = destinationUser;
    }
}
