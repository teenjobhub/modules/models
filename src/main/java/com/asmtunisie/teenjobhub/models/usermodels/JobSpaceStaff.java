package com.asmtunisie.teenjobhub.models.usermodels;

import org.springframework.data.mongodb.core.mapping.DBRef;

public class JobSpaceStaff {

    @DBRef
    private UserProfile userProfile;
    private boolean pending;


    public JobSpaceStaff(UserProfile userProfile, boolean pending) {
        this.userProfile = userProfile;
        this.pending = pending;
    }

    public UserProfile getUserProfile() {
        return userProfile;
    }

    public void setUserProfile(UserProfile userProfile) {
        this.userProfile = userProfile;
    }

    public boolean isPending() {
        return pending;
    }

    public void setPending(boolean pending) {
        this.pending = pending;
    }
}
