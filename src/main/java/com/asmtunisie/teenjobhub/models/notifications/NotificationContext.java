package com.asmtunisie.teenjobhub.models.notifications;


public enum NotificationContext {
    CONNEXION,
    JOBSPACE,
    JOBPOST
}
