package com.asmtunisie.teenjobhub.models.chat;

import com.asmtunisie.teenjobhub.models.usermodels.UserProfile;
import org.springframework.data.annotation.Reference;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;

public class ChatRoomSubscription {

    @Reference
    @Indexed
    private UserProfile userProfile;

    @Reference
    private ChatMessage lastSeenMessage;

    public UserProfile getUserProfile() {
        return userProfile;
    }

    public void setUserProfile(UserProfile userProfile) {
        this.userProfile = userProfile;
    }

    public ChatMessage getLastSeenMessage() {
        return lastSeenMessage;
    }

    public void setLastSeenMessage(ChatMessage lastSeenMessage) {
        this.lastSeenMessage = lastSeenMessage;
    }
}
